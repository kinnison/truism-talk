title: Storytime with Daniel
class: animation-fade
layout: true

.bottom-bar[
{{title}}
]

---

count: false

# Leader slide

- Press 'b' to toggle 'blackout mode'
- Press 'p' to toggle 'presenter mode'
- Press 'c' to create a clone of this window which will keep up
- Press left/right to move around slides
- The next slide is where the presentation begins

???

This is where presenter notes appear

---

count: false
title: Formatting examples

.row[

.col-6[

# Header 1

## Header 2

## Header 3

]

.col-6[

Hello world, this text belongs in the second column

]

]

```rust
fn rust_code(from: &str) -> Result<(), ()> {
    todo!()
}
```

---

class: impact

## Storytime with Daniel

???

- Indulge me, let me impart some truths
- Each in the form of a small vignette
- The core content of this comes from a series of articles eight years ago
- Each is presented as a truism, though if you think more deeply there is value there
- Each will give you homework. Make sure you have something to take notes
- You can think of this as a somewhat filtered set of thoughts on the commandments
  from a more FOSS point of view.
- While I will talk mostly about personal projects and efforts, these stories
  apply just as well to your work life.

---

class: impact2

## If you don't know why you're doing it,

## you shouldn't be doing it.

???

- Critical part of any project, or any activity you choose to undertake
- If you have no reason to do something, why bother to do it at all
- Often "no reason" turns out to be false, we do have reasons
- They might be spurious, or facetious, but they do exist

---

class: impact2

## If you don't know why you're doing it,

## you shouldn't be doing it.

???

- Formally - without a well defined requirement you should do nothing.
- Without a well defined criterion to assess whether or not the requirement is met, you should do nothing.
- In the world of writing code for fun, there need be no more complex a requirement than "I fancy doing it"
- As an example I once wrote a full implementation of a regular expression engine in pure Lua
  "for fun" simply because I wanted to better understand DFAs and NFAs, and how they can be used to parse text
- Your homework: Whatever you're up to in the next few days to weeks, take a moment to first thing about why you're doing it, and how you'll know if you've achieved that goal.

---

class: impact

## If you don't have a plan,

## you will wander in the reeds

???

- When I first set out to write Gitano
  - missed the ease of publishing bzr repos
  - looked at alternatives and didn't like them for whatever reason
  - knew I wanted it to be "nice"
- I had no plan for how to achieve it
- Indeed I started coding under a name which turns out to have been taken already, meaning I had to rename the project before its first ever release

---

class: impact

## If you don't have a plan,

## you will wander in the reeds

???

- Being a seasoned hacker, I immediately started shaving yaks
- Utility code to interact with git repositories
- No cohesive plan for how that might look or work
- Took a long time to dig back out of that hole
- Eventually made a MVP plan, which really helped.

---

class: impact

## If you don't have a plan,

## you will wander in the reeds

???

- Plans do not, and will not, spring into being fully formed
- You can make progress with an incomplete plan, so long as you know its gaps
- Nebulous plans "I will hack and see what happens" are fine - prototyping works this way
- Be aware of what you want to achieve and how a nebulous plan may affect the timing of that achievement.
- Strongly suggest bulleted lists of pretty high level requirements at first

---

class: impact

## If you don't have a plan,

## you will wander in the reeds

???

1. Build git abstraction so I can read my configuration
2. Add push and pull over ssh
3. Add admin commands over ssh
4. Add some kind of gitweb support

- Things didn't go to plan, or in that worder, but the goals were eventually achieved.
- This wasn't released, but I did immediately use it to host itself.

---

class: impact

## If you don't have a plan,

## you will wander in the reeds

???

- In addition to actions to perform, or features to write, plans may include architectural decisions
- What language will you write it in? Will it be a CLI or web application, what tools will you use?
- No plan survives contact with the enemy - expect to have to change and adapt, adjust and refocus.
- Homework - Pick a project which isn't progressing well, maybe FOSS, maybe DIY, who knows. Write
  down a two to five point plan to achieve some goal in that project, and see if it helps you get
  to your goal more quickly.

---

class: impact2

## If you haven't pushed it to revision control,

## it doesn't exist.

???

- Long long ago, tarballs/zipfiles as "releases"
- Then RCS, SCCS, then CVS, SVN, TLA/BAZ, BZR, Git, etc.
- When I started, long long ago, tarball (zipfile) method.
- Put that on a website, hope someone else downloads it. Revision control by version numbering the tarballs
- This approach worked, I managed to spelunk some ancient Pocket-C I wrote, and then had fun rewriting it in
  Haskell some years back. It's pushed to revision control elsewhere, perhaps you can find it and fix my
  horrible bugs in it.
- Keeping old versions of code can be instructive.
  - Remember why things were done
  - See variants of features which were discarded for whatever reason
  - Recover incorrectly discarded code

---

class: impact2

## If you haven't pushed it to revision control,

## it doesn't exist.

???

- Some days I'm sad that the floppy discs of my first ever programming are lost to the sands of time.
- Data archaeology only works, if somewhere there's still a copy of the code
- In summary, if your code is not in revision control, you're missing on a lot, even if you're the only person working on the code.
- More importantly though, if you've not pushed that content somewhere else too, then you run a greater risk of loss, because data _is_ fragile.
- Homework:
  - If you already do VCS, go find something old, browse the code and history, and enjoy reminiscing over how clever/stupid your past self was.
  - If you're not yet a VCS fanatic, find something you love or think is useful that you've written, imagine how sad you'd be if it were lost forever, then commit it and push it somewhere for others to admire.

---

class: impact

## If it's not tested,

## it doesn't work.

???

- Many software devs think testing is a dirty word.
- Interestingly, in commercial engagements, it used to be common for testing to not be considered worthwhile since the value is not very visible and hard for some to understand.
- Testing, at whatever level you feel you can achieve, is **always** worthwhile.
- Somewhat controversial statement, but here's my justification:
  - I have never been in a situation where I was unhappy about tests existing.
  - Sometimes they were very restrictive making it hard to change how code behaved
  - But if that was the intent of the test author, then the tests did their job well.

* Plenty of ways to test software, e.g. human testing, unit testing, integration testing, scenario testing
* Most importantly, no matter how testing is to be done, the tests must be written down in some manner.

---

class: impact

## If it's not tested,

## it doesn't work.

???

- Consistency between test runs is critical
- For human testing, that involves clear and precise test descriptions / ticklists
  - Not specific to a particular computer, describing how to indicate where results deviate from expectations
- For automated tests, ensuring there are no outside influences overriding test conditions
- Ensuring external interfaces are mocked is a way to achieve this
- Running in controlled isolation (e.g. containers or VMs)
- Modern CI systems try to help with this

---

class: impact

## If it's not tested,

## it doesn't work.

???

- Worth noting that not everything can be tested in isolation
- Sometimes you need a wider set of systems to test effectively
- Try to control those systems so that you can make them consistent and reproducible
- Anyone should be able to run your tests and satisfy themselves of the correctness of the program if possible
- Anything not covered in testing is a potential thing to go wrong and is thus a candidate for removal from the software
- Sometimes you cannot cover every single line of code (e.g. defensive programming against hard-to-mock environmental errors)
- Exercise discretion
- The big takeaway is that if you are not systematically testing your code, bugs _will_ creep in and bite you when you least expect them
- Homework: Find a project of yours which is insufficiently tested and write some tests. Even if all your new tests pass because the code was perfect, you'll now be more confident that you can make changes to the codebase and not break things, or at least to know if you do.

---

class: impact2

## If it's not backed up,

## you will lose it.

???

- You may think that we've already touched on this with revision control
- VCS is one form of backup
- But if the VCS server isn't backed up, what happens if your server fails? Do you now lose any project you didn't have checked out on your laptop?
- Unless you're Linus Torvalds, too few people have copies of your repository to consider it safe without proper backups

---

class: impact2

## If it's not backed up,

## you will lose it.

???

- Losing your code is only one thing which can go wrong for a developer when a drive fails
- Lots of other things are on your system, such as its configuration, your dotfiles, etc.
- Are they less worthy of protection than your code.
  - The astute among you may realise my dotfiles (and perhaps yours) may already be pushed to revision control.
- If you think you'd be sad if you lost it, it should be backed up.

---

class: impact2

## If it's not backed up,

## you will lose it.

???

- Backup early, backup often
- If your backups aren't tested, they don't work - try to restore your backup and check it works
- Without testing restoration from backup, they're just a pile of data, not a recovery plan for the future
- Homework: Go find something you don't keep regularly backed up and think:
  - "If I were to `rm -rf` this, right now, would I then be upset?"
  - If "no", do so.
  - If "yes", find some way to change it to a "no". Perhaps by deriving, implementing, and verifying a backup strategy for it.

---

class: impact

## If you are not measuring it,

## you don't know how well it's doing.

???

- The desire to make something faster, smaller, more efficient is a common failing among programmers.
- For the most part, programmers don't actually know for certain what they want to make better, or if it's worth their time.
- You may have a goal to do this as an exercise in learning optimisation, or adding some feature.
- Generally these efforts are usually premature because programmers love to code and solve perceived problems, rather than stepping back to measure

---

class: impact

## If you are not measuring it,

## you don't know how well it's doing.

???

- You can't always measure objectively, particularly some "level" of functionality
- It's always possible to think of extra "nice to have"s.
- If you can't point at a need - a requirement, perhaps you don't need to do it though
- Making something faster is only worthwhile if how it currently performs is insufficient for some rapidity requirement.
- Let's assume you have such a requirement, how do you determine where to focus effort?

---

class: impact

## If you are not measuring it,

## you don't know how well it's doing.

???

- Number of pieces of software to help - profilers and the like.
- Higher level, or dynamic languages often have nice language level profilers
- Lower level or static languages often use machine-level profilers such as `prof` or `perf`.
- Without such a tool, you cannot be confident of understanding where in your code time is being spent
- Without such an understanding you cannot usefully direct your efforts to improve matters

---

class: impact

## If you are not measuring it,

## you don't know how well it's doing.

???

- Once, a long time ago, I was looking at some code I was convinced ought to be more performant than it was
- Running several tests with profilers led me to discover an assumption I'd made about the code was entirely wrong
- The profiles led me to see that effort I was convinced was being cached was in fact not.
- Four extra lines of code (in a codebase of many thousands of lines) and the complexity of the code dropped
  from O(n<sup>m</sup>) to O(n).
- Without that profiling effort, I would never have found this as I had already assumed the cache was in place.
- Homework: Measure the performace of a project of yours which doesn't behave as well as you'd hoped
  - Profile it, perhaps you'll find a nice quick fix
  - If your software is already perfect (meets its requirements) why not look for something else to profile and help with
  - Maybe you'll make a useful FOSS contribution, even if it's just identifying a hotspot for the developer to try and fix.

---

class: impact2

## If you wouldn't want to use it yourself,

## why would anyone else?

???

- Often the case that you wake up in the night, or lunch, or meeting, or whatever, excited about some new software which if you only wrote it would change the world.
- You imagine it'll be amazing and that everyone will want to use it.
- It rarely is, they rarely do.
- One of the best tests to apply is "Would I want to use this?"
- The effort you put into any project must be of value to someone.
  - Earlier I spoke about how you should know why you're doing something.
  - The longer-term a project becomes, the more important it will be that one of your reasons is that it solves a real problem for you.
  - Otherwise you'll lose interest.
  - The someone to whom the project has value really must include yourself at some level.

---

class: impact2

## If you wouldn't want to use it yourself,

## why would anyone else?

???

- Another aspect to consider is simply that for people to want to use something, it needs to be nice to use.
- Unless your target audience is significantly different from yourself, that means it needs to be nice for **you** to use.
- User experience is hard enough to think about at the best of times, but if you cannot imagine yourself as the user, it'll only be even harder.
- FOSS hackers often lack the luxury to pay someone else to tell them how something shoudl be used.
- The only UX you can fully grok is your own, so design for yourself first.

---

class: impact2

## If you wouldn't want to use it yourself,

## why would anyone else?

???

- A corollary of this is, if you've lost interest, pass your project on, or drop it.
- I've been in this position a number of times. There's code out there maybe some of you are using
  which I wrote but no longer assert ownership of.
- I'm glad I wrote it, as are many others, but I'm equally glad to be rid of it because I no longer want or need to use it.
- Homework: Go over your active projects, decide if there are any you no longer use.
  - If so, find someone else to hand it off to.
  - Don't necessarily give it up entirely, but if someone else who uses it wants to run the project, let them
  - That way you can contribute and help others, but you're no longer responsible for making it nice to use etc.
  - Regardless of finding such projects or not, revisiting projects and mothballing some can be cathartic,
  - Freeing brain space you didn't even reaise was in use.
  - Also, you might find something you forgot, which really solves a problem which you had been planning to solve with that amazing program you thought up in the middle of the night last week.

---

class: impact

## If you have no plan to maintain it,

## it will cease working.

???

- My final vignette for today covers the fact that the world is ever changing
- If software doesn't change, it may simply cease to work in the new world.
- We call that bit-rotting.
- All rules have exceptions, and there may be software considered "done" "perfect" "finished".
- I would call many of those projects "risky" "stagnant" "dead" "uninteresting".
- Even if your project is perfect **now**, it will not remain so indefinitely.

---

class: impact

## If you have no plan to maintain it,

## it will cease working.

???

- Everything is changing around you
- Your target OS is changing, at the fundamentals (kernel, filesystem, etc)
- Or perhaps simply in terms of user experience expectations
- Consider something like FDSDK - all components will be updated in that regularly as things progress.
- Software which does not react to this ever-changing landscape will eventually fail to work.

---

class: impact

## If you have no plan to maintain it,

## it will cease working.

???

- Additionally, expectations and needs will change and evolve.
- Expectations of usability, security, functionality, documentation, blahblahblah change over time
- Not just within users, but with respect to the total set of users.
- If you have no plan to deal with issues these changes expose, no interest in fixing, maintaining, or extending your project
  then it will eventually be overtaken by some new upstart project whose developers have energy and interest to drive them.
- Don't let this happen to a project you still care about.

---

class: impact

## If you have no plan to maintain it,

## it will cease working.

???

- One final example from me
- A few years ago I wrote a program to extract entropy from sound cards
- I did this because my server lacked good entropy sources and kept blocking for SSH keys etc.
- This worked fine, and I released it for others to use
- A number of years later, APIs changed, and the program stopped being able to be compiled.
- I'd not noticed this, nor did I care, because I no longer needed the software to be updated, my device was old and not supported by newer kernels etc.
- I realised I lacked a plan for maintaining, but fortunately someone else wanted to fix and maintain the software so I handed it off to them.
- They continue to rely on it to this day and maintain it for others.
- It wasn't that I was neglectful per-se, simply I had no use or interest in the software, so handing it off was the right thing to do.

---

class: impact

## If you have no plan to maintain it,

## it will cease working.

???

- At this point, if you do all the homeworks in order, your project list will be honed down to:
  1. Projects you have a reason to work on
  2. that you know what you're doing to achieve your goals
  3. that are in revision control and are pushed to a server
  4. that are well tested
  5. that are backed up
  6. have appropriate performance measurements in place
  7. and that you yourself actively use and care about.
- Homework: Simply keep on top of that list, keep it at the high quality you will have achieved through applying these truisms, and continue to write new and cool stuff which satisfies you, demonstrates your capabilities, and keeps on working.

---

class: impact2

## Go forth, and engineer the future

## (so that I don't have to)

???

Any questions?
